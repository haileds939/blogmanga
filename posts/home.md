# BlogManga

BlogManga is full front-end, markdown based blog that doesn't require a build system. The markdown files are fetched from the static folder, and rendered on the fly, with client side code.

## With Netlify CMS

With Netlify CMS, you can manage your content using a neat GUI. If you are not using this, you may delete the 'admin' folder

## Fork and enjoy!

<https://github.com/kd96/blogmanga>
